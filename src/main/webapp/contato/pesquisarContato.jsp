
<%@page import="br.com.senac.agenda.model.Usuario"%>
<%@page import="java.util.List"%>
<jsp:include page="../header.jsp"/>





<% List<Usuario> lista = (List) request.getAttribute("lista"); %> 
<% String mensagem = (String) request.getAttribute("mensagem"); %>

<% if (mensagem != null) {%>
<div class="alert alert-danger">
    <%= mensagem%>
</div>
<%}%>

<fieldset  >
    <legend>Pesquisa de Contato</legend>

    <form action="">

        <div class="row">
            <div class="form-group col-2">
                <label for="txtCodigo" style="margin-right: 10px">C�digo:</label> 
                <input id="id" name="id" class="form-control form-control-sm" id="txtCodigo" type="text" />
            </div>
            <div class="form-group  col-6">
                <label for="nome" >Nome:</label> 
                <input name="nome" id="nome" class="form-control form-control-sm" type="text" />
            </div>
            <div class="form-group  col-2">
                <label for="estado" >Estado:</label> 
                <select id="estado" class="form-control form-control-sm" name="estado">
                    <option value="" selected >--</option>
                    <option value="ES" >ES</option>
                    <option value="SP">SP</option>
                    <option value="RJ">Three</option>
                </select>
            </div>
            <div class="form-group  col-2" style="display: flex;align-items: flex-end;">
                <button  type="submit" class="btn btn-default" >
                    <i class="fa fa-search"></i>  Pesquisar
                </button>
            </div>
        </div>







    </form>
</fieldset>

<hr />


<table class="table table-striped" >
    <thead>
        <tr>
            <th >C�digo</th><th >Nome</th><th >Endere�o</th><th >Telefone</th>
            <th >Celular</th><th>Fax</th><th >E-mail</th> <th></th>
        </tr>
    </thead>

    <tr>
        <td>1</td><td>Jos� da Silva</td><td>Rua J , N� 01 , Bento Ferreira - Vitoria - ES</td>
        <td>(27)3333-3333</td><td>(27)9999-9999</td><td>(27)4444-4444</td><td>jose@silva.com</td>
        <td>

            <a href="./SalvaContatoServlet?codigo=1">

                <img src="../resources/imagens/contato/edit.png"

            </a>
            <a href="./SalvaContatoServlet?codigo=1" data-toggle="modal" data-target="#exampleModal">

                <img src="../resources/imagens/contato/remove.png"

            </a>

           

        </td>
    </tr>
    <tr>
        <td>2</td><td>Jos� da Oliveira</td><td>Rua J , N� 01 , Bento Ferreira - Vitoria - ES</td><td>(27)3333-3333</td><td>(27)9999-9999</td><td>(27)4444-4444</td><td>jose@silva.com</td>
        <td>

            <a href="./SalvaContatoServlet?codigo=2">Editar</a>

        </td>
    </tr>
    <tr>
        <td>3</td><td>Jos� da Mattos</td><td>Rua J , N� 01 , Bento Ferreira - Vitoria - ES</td><td>(27)3333-3333</td><td>(27)9999-9999</td><td>(27)4444-4444</td><td>jose@silva.com</td>
        <td>
            <a href="./SalvaContatoServlet?codigo=3">Editar</a>
        </td>
    </tr>


</table>


 <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Launch demo modal
            </button>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Exclus�o</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Deseja realmente apagar o contato <span id="nomeContato"></span> ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary">Confirmar</button>
            </div>
        </div>
    </div>
</div>







<jsp:include page="../footer.jsp"/>