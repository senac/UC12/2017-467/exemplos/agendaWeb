
<%@page import="br.com.senac.agenda.model.Estado"%>
<%@page import="br.com.senac.agenda.model.Contato"%>
<%@page import="br.com.senac.agenda.dao.EstadoDAO"%>
<%@page import="java.util.*"%>
<jsp:include page="../header.jsp"/>

<%
    Contato contato = (Contato)request.getAttribute("contato");
    String mensagem = (String) request.getAttribute("mensagem");
    String erro = (String) request.getAttribute("erro");
    EstadoDAO dao = new EstadoDAO() ; 

    List<Estado> lista = dao.listar(); 

%>

<% if (mensagem != null) {%>
<div class="alert alert-success" role="alert">
    <%= mensagem%>
</div>
<%}%>

<% if (erro != null) {%>
<div class="alert alert-danger" role="alert">
    <%= erro%>
</div>
<%}%>



<form  method="post" action="./SalvaContatoServlet">

    
   


    <div class="row">
        <div class="col-sm">
            <div class="form-group">
                <label for="codigo">C�digo</label>
                <input type="text" class="form-control col-2" 
                       id="codigo" name="codigo" readonly 
                       value="<%= contato != null ? contato.getId()  : "" %>"
                       >
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            <div class="form-group">
                <label for="nome">Nome</label>
                <input type="text" 
                       class="form-control" 
                       id="nome" placeholder="nome" name="nome" 
                        value="<%= contato != null ? contato.getNome(): "" %>"
                       >
            </div>
        </div>
    </div>

    <div class="row">
        
        <div class="col-sm-12 col-lg-4">
            <div class="form-group">
                <label for="nome">Telefone</label>
                <input type="text" class="form-control" id="nome" placeholder="nome" name="telefone"  >
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="nome">Celular</label>
                <input type="text" class="form-control" id="nome" placeholder="nome" name="celular"  >
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="nome">Fax</label>
                <input type="text" class="form-control" id="nome" placeholder="nome" name="fax"  >
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                <label for="nome">CEP</label>
                <input type="text" class="form-control" id="nome" placeholder="nome" name="cep"  >
            </div>
        </div>
        <div class="col-sm-7">
            <div class="form-group">
                <label for="nome">Endere�o</label>
                <input type="text" class="form-control" id="nome" placeholder="nome" name="endereco"  >
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label for="nome">Numero</label>
                <input type="text" class="form-control" id="nome" placeholder="nome" name="numero"  >
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-sm-4">
            <div class="form-group">
                <label for="nome">Bairro</label>
                <input type="text" class="form-control" id="nome" placeholder="nome" name="bairro"  >
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="nome">Cidade</label>
                <input type="text" class="form-control" id="nome" placeholder="99.999-999" name="cidade"  >
            </div>
        </div>

        <div class="col-sm-2">
            <div class="form-group">
                <label for="nome">UF</label>
               
                 <select id="estado" class="form-control form-control-sm" name="estado">
                   <option value="" selected >--</option>
                   <% for( Estado e : lista ){%>
                    <option value="<%= e.getSigla()%>" ><%= e.getNome() %></option>
                    <%}%>
                </select>
                
                
                
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label for="nome">E-mail</label>
                <input type="text" class="form-control" id="nome" placeholder="nome" name="email"  >
            </div>
        </div>

    </div>






    <div class="col-sm">
        <div class="form-group text-right">
            <button type="submit" class="btn btn-primary">Salvar</button>
            <button type="reset" class="btn btn-danger">Cancelar</button>
        </div>
    </div>
</form>





<jsp:include page="../footer.jsp"/>