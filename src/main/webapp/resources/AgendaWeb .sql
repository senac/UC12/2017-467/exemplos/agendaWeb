create database IF not EXISTS agendaWeb ; 
use agendaWeb  ; 



DROP TABLE IF EXISTS usuario;

CREATE TABLE usuario (
  id int(11) NOT NULL AUTO_INCREMENT  PRIMARY KEY , 
  nome varchar(200) NOT NULL,
  senha varchar(8) NOT NULL
 
) ;

insert into  usuario (nome, senha) value ('daniel', '123');
insert into  usuario (nome, senha) value ('silva', '568');
insert into  usuario (nome, senha) value ('ana', '852');

DROP TABLE IF EXISTS contato;

CREATE TABLE contato (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY , 
  nome VARCHAR(200) NOT NULL,
  celular VARCHAR(20) NULL,
  telefone VARCHAR(20) NULL,
  fax VARCHAR(20) NULL,
  cep VARCHAR(20) NULL,
  endereco VARCHAR(200) NULL,
  numero INT NULL,
  bairro VARCHAR(200) NULL,
  cidade VARCHAR(200) NULL,
  uf VARCHAR(45) NULL,
  email VARCHAR(200) NULL);

  
INSERT INTO contato (id, nome, celular, telefone, cep, endereco, numero, bairro, cidade, uf, email)
VALUES ('1', 'Jose da Silva', '(27)9999-7777', '(27) 3333-5555', '29160200', 'Rua J', '1', 'São Jose', 'Serra', 'ES', 'jose@silva.com');

INSERT INTO contato (id, nome, celular, telefone, cep, endereco, numero, bairro, cidade, uf, email)
VALUES ('2', 'Miguel da Silva', '(27)9999-7777', '(27) 3333-5555', '29160200', 'Rua J', '1', 'São Jose', 'Serra', 'ES', 'Miguel@silva.com');

INSERT INTO contato (id, nome, celular, telefone, cep, endereco, numero, bairro, cidade, uf, email)
VALUES ('3', 'Jair da Oliveira', '(27)9999-7777', '(27) 3333-5555', '29160200', 'Rua J', '1', 'São Jose', 'Serra', 'ES', 'jair@Oliveira.com');

INSERT INTO contato (id, nome, celular, telefone, cep, endereco, numero, bairro, cidade, uf, email)
VALUES ('4', 'Angela Rosa', '(27)9999-7777', '(27) 3333-5555', '29160200', 'Rua J', '1', 'São Jose', 'Serra', 'ES', 'angela@rosa.com');

INSERT INTO contato (id, nome, celular, telefone, cep, endereco, numero, bairro, cidade, uf, email)
VALUES ('5', 'Jose Gomes', '(27)9999-7777', '(27) 3333-5555', '29160200', 'Rua J', '1', 'São Jose', 'Serra', 'ES', 'jose@gomes.com');

INSERT INTO contato (id, nome, celular, telefone, cep, endereco, numero, bairro, cidade, uf, email)
VALUES ('6', 'Araujo Marques', '(27)9999-7777', '(27) 3333-5555', '29160200', 'Rua J', '1', 'São Jose', 'Serra', 'ES', 'araujo@marques.com');
