
<%@page import="br.com.senac.agenda.model.Usuario"%>
<%@page import="java.util.List"%>
<jsp:include page="../header.jsp"/>


<script type="text/javascript">

function excluir(id , nome){
    
    $('#nomeUsuario').text(nome);
    
    var formNome = $('#txtNome').val(); 
    var formId = $('#txtCodigo').val() ; 
    
    var url = './ExcluirUsuarioServlet?id=' + id + "&formNome=" + formNome + "&formId=" + formId ; 
    
    $('#btnConfirmar').attr('href' , url ) ; 
    
}
    
    
</script>
    


<% List<Usuario> lista = (List) request.getAttribute("lista"); %> 
<% String mensagem = (String) request.getAttribute("mensagem"); %>

<% if (mensagem != null) {%>
<div class="alert alert-danger">
    <%= mensagem%>
</div>
<%}%>

<fieldset  >
    <legend>Pesquisa de Usu�rios</legend>
    <form class="form-inline" action="./PesquisaUsuarioServlet">
        <div class="form-group" style="padding: 20px;">
            <label for="txtCodigo" style="margin-right: 10px">C�digo:</label> 
            <input name="id" class="form-control form-control-sm" id="txtCodigo" type="text" />
        </div>
        <div class="form-group">
            <label for="nome" style="margin-right: 10px">Nome:</label> 
            <input name="nome" id="txtNome" class="form-control form-control-sm" type="text" />
        </div>
        <div class="form-group">
            <button style="margin-left: 10px" type="submit" class="btn btn-default" >
                <i class="fa fa-search" aria-hidden="true"></i>Pesquisar
            </button>

            <a href="./gerenciarUsuario.jsp" style="margin-left: 10px" class="btn btn-success" role="button" aria-disabled="true">
                <i class="fa fa-plus" aria-hidden="true"></i>Adicionar
            </a>
            
            <a class="btn btn-danger">
              teste   <i class="fa fa-user-plus" aria-hidden="true"></i>   
            </a>
         
        </div>
    </form>
</fieldset>

<hr />


<table class="table table-hover">
    <thead>
        <tr>
            <th>C�digo</th> <th>Nome</th> <th></th>
        </tr>
    </thead>

    <% if (lista != null && lista.size() > 0) {
            for (Usuario u : lista) {
    %>
    <tr>
        <td><%= u.getId()%></td><td><%= u.getNome()%></td> 

        <td style="width: 120px;">
            <a href='./SalvarUsuarioServlet?id=<%= u.getId() %>'   style="margin-right: 10px;" >
                <img src="../resources/imagens/usuario/edit.png" />
            </a>
                <a href="" data-toggle="modal" data-target="#modalExclusao" 
                   onclick="excluir(<%= u.getId()%> , '<%= u.getNome()%>' );">
                <img src="../resources/imagens/usuario/remove.png" />
            </a>
        </td>

    </tr>

    <% } // for

    } else {%>

    <tr >
        <td  colspan="2">N�o Existem registros.</td>
    </tr>

    <%}%>


</table>



<!-- Modal -->
<div class="modal fade" id="modalExclusao" tabindex="-1" role="dialog" 
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Exclus�o</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Deseja realmente apagar o usu�rio <span id="nomeUsuario"></span> ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                
                <a  id="btnConfirmar" class="btn btn-primary">Confirmar</a>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../footer.jsp"/>