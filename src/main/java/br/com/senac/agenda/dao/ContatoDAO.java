package br.com.senac.agenda.dao;

import br.com.senac.agenda.model.Contato;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ContatoDAO extends DAO<Contato> {

    @Override
    public void salvar(Contato contato) {

        Connection connection = null;

        try {

            String query = null;

            if (contato.getId() == 0) {
                query = "insert into contato  (nome ,celular ,fax , telefone , cep , endereco , numero , bairro , cidade,uf , email) values (? , ? , ? , ?, ?, ?, ? , ? , ? , ? , ? ) ";
            } else {
                query = "update  contato  set nome = ? , celular = ?,  fax = ? , telefone= ? , cep  = ? , endereco = ? , numero = ? ,bairro = ? , cidade = ? , uf = ? , email = ? where id = ?  ";
            }

            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setString(1, contato.getNome());
            ps.setString(2, contato.getFax());
            ps.setString(3, contato.getTelefone());
            ps.setString(4, contato.getCelular());
            ps.setString(5, contato.getCep());
            ps.setString(6, contato.getEndereco());
            ps.setInt(7, contato.getNumero());
            ps.setString(8, contato.getBairro());
            ps.setString(9, contato.getCidade());
            ps.setString(10, contato.getUf());
            ps.setString(11, contato.getEmail());
            if (contato.getId() != 0) {
                ps.setInt(12, contato.getId());

            }

            ps.executeUpdate();

            if (contato.getId() == 0) {
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                contato.setId(rs.getInt(1));
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void deletar(Contato contato) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Contato> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Contato get(int id) {
        Contato contato = null;
        Connection connection = null;

        try {

            String query = "SELECT * FROM contato WHERE ID = ? " ;
            connection = Conexao.getConnection() ; 
            
            PreparedStatement ps = connection.prepareStatement(query) ; 
            
            ps.setInt(1, id);
            
            ResultSet rs =  ps.executeQuery() ; 
            if(rs.first()){
                contato = new Contato();
                contato.setId(rs.getInt("id"));
                contato.setNome(rs.getString("nome"));
                
            }
            
            

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Erro ao fechar conexao...");
            }
        }

        return contato;
    }

}
