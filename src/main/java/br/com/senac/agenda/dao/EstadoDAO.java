package br.com.senac.agenda.dao;

import br.com.senac.agenda.model.Estado;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EstadoDAO extends DAO<Estado> {

    @Override
    public void salvar(Estado objeto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deletar(Estado objeto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Estado> listar() {
        List<Estado> lista = new ArrayList<>() ; 
        
        String query = "Select * from estado ";
        Connection connection = null;
        try {
            
            connection  =  Conexao.getConnection();
            Statement statement = connection.createStatement() ; 
            
            ResultSet rs = statement.executeQuery(query);
            while(rs.next()){
                Estado estado = new Estado();
                estado.setId(rs.getInt("id"));
                estado.setNome(rs.getString("nome"));
                estado.setSigla(rs.getString("sigla"));
                lista.add(estado);
            }
            

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return  lista;
    }

    @Override
    public Estado get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
