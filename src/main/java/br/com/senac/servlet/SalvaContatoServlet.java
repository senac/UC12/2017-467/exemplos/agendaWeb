/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.servlet;

import br.com.senac.agenda.dao.ContatoDAO;
import br.com.senac.agenda.dao.UsuarioDAO;
import br.com.senac.agenda.model.Contato;
import br.com.senac.agenda.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sala302b
 */
@WebServlet(name = "SalvaContatoServlet", urlPatterns = {"/contato/SalvaContatoServlet"})
public class SalvaContatoServlet extends HttpServlet {



    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       String codigo = request.getParameter("codigo");
       String erro  = null ; 
        
        Integer id = null;

        if (codigo != null && !codigo.isEmpty()) {
            id = Integer.parseInt(codigo);
        }else{
            erro = "Erro buscar contato;" ; 
        }
        
        ContatoDAO dao = new ContatoDAO() ; 
        
        Contato contato = dao.get(id) ;
        
        request.setAttribute("contato", contato);
        request.setAttribute("erro", erro);
        
         RequestDispatcher dispatcher = request.getRequestDispatcher("gerenciarContato.jsp");
        //voltar a pagina
        
        dispatcher.forward(request, response);
        
        
        
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String mensagem = null ; 
        String erro = null ; 
        
        String codigo = request.getParameter("codigo");
        String nome = request.getParameter("nome");
        String telefone = request.getParameter("telefone");
        String celular = request.getParameter("celular");
        String fax = request.getParameter("fax");
        String cep = request.getParameter("cep");
        String endereco = request.getParameter("endereco");
        String n = request.getParameter("numero");
        String bairro = request.getParameter("bairro");
        String cidade = request.getParameter("cidade");
        String uf = request.getParameter("uf");
        String email = request.getParameter("email");

        Integer id = null;
        Integer numero = null;

        if (codigo != null && !codigo.isEmpty()) {
            id = Integer.parseInt(codigo);
        }else{
            id = 0 ;
        }

        if (n != null && !n.isEmpty()) {
            numero = Integer.parseInt(n);
        }else{
            numero = 0  ;
        }
        
        Contato contato = new Contato() ; 
        contato.setId(id);
        contato.setNome(nome);
        contato.setTelefone(telefone);
        contato.setFax(fax);
        contato.setCelular(celular);
        contato.setCep(cep);
        contato.setEndereco(endereco);
        contato.setBairro(bairro);
        contato.setNumero(numero);
        contato.setCidade(cidade);
        contato.setEmail(email);
        contato.setUf(uf);
        try{
        // salvar banco 
        ContatoDAO dao = new ContatoDAO();
        
        dao.salvar(contato);
        
        mensagem = "Salvo com sucesso." ; 
        
        request.setAttribute("mensagem", mensagem );
        request.setAttribute("contato", contato);
        }catch(Exception ex){
            erro = "Falha ao salvar." ; 
            request.setAttribute("erro", erro );
        }
        
        RequestDispatcher dispatcher = request.getRequestDispatcher("gerenciarContato.jsp");
        //voltar a pagina
        
        dispatcher.forward(request, response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
